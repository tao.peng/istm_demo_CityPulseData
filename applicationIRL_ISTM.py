#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 31 13:06:48 2018

@author: taopeng
"""

from class_ISTM import ProcessIRL

import numpy as np
backTime = 6 #回溯时间 regarder en arrière dans le temps, combien de point du temps

#listTauxTrain = [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
#listTauxTrain = [0.3]

listTauxTrain = [0.3]
#listTrou = [5, 10, 15, 20 ,25]
listTrou = [5]

test_or_non = 'yes' # En mode ‘test’, le programme n'exécute qu'une partie des données pour gagner du temps. 在测试模式下，程序进行到30%时会终止，以解约时间 
#test_or_non = 'Non' # 

'''
0 ISTM
1 ISTM_OPM
2 Dynamique_but_stupide_time_space_model
3 STATIC_TIME_SPACE
4 STATIC_TIME
5 STatiC_SPACE
6 Moyenne
7 PrivousValue
8 ISTM_OPM_fraiche
'''
nameMethods = ['ISTM','ISTM_OPM','Dynamic_Space-Time-Based_Model','Static_Space-Time-Based_Model','Static_Time-Based_Model','Static_Space_Based_Model','Mean','Previous_Data','ISTM_OPM_fraiche']
canditatureMethods =  [0]

listVoisinNumero =  np.load("metaDonneeCityPulse/tousLesVoisinsDeTouslesPionts.npy", allow_pickle=True).tolist()#第一列是本节点名，后续是邻居节点名 La première colonne est le nom de ce nœud, suivi des noms des nœuds voisins
#listVoisinNumero = np.load("newEXP_RH/simulationDonnees//tousLesVoisinsDeTouslesPionts_RH_TH.npy", allow_pickle=True).tolist()#第一列是本节点名，后续是邻居节点名 La première colonne est le nom de ce nœud, suivi des noms des nœuds voisins



#读取 E_original方便以后比较
E_original  = np.load("metaDonneeCityPulse/E_origninal.npy").tolist()
for indexMethods in canditatureMethods:
    print ("le method est :" + nameMethods[indexMethods])
    table_score=[] 
    table_time=[] 
    for tauxTrain in listTauxTrain:  
        score_taux_special = []
        time_taux_special = []
        for trou in listTrou:
            E_special = np.load('simulationDonneesCityPluse/E'+str(trou)+'_tauxTrain'+str(int(tauxTrain*100))+'.npy').tolist()
            print ("% SMV est " + str(trou) + ", % utilisée pour l'initialisation = " + str(int(tauxTrain*100)))
            methode = ProcessIRL(E_original,E_special,tauxTrain,trou,backTime,listVoisinNumero,testModel=test_or_non)
            score,time = (0,0)
            if indexMethods == 0:
                methode.trainFirst_ISTM()
                time, score = methode.prosser_unParUn_jusquaFini_ISTM()#ISTM
            elif indexMethods == 1:
                methode.trainFirst_ISTM()
                time, score = methode.prosser_unParUn_jusquaFini_ISTM_OPM()#ISTM_OPM
                
            elif indexMethods == 2:
                methode.trainFirst_Dymanic_TRA_opm_time_space_model()
                time, score = methode.prosser_unParUn_jusquaFini_Dymanic_TRA_opm_Time_space_based_model()#Dynamique_but_stupide_time_space_model
            elif indexMethods == 3:
                methode.trainFirst_Static_time_space_model()
                time, score = methode.prosser_unParUn_jusquaFini_STATIC_Time_space_based_model()#STATIC_TIME_SPACE
            elif indexMethods == 4:
                methode.trianFirst_time_based_model()
                time, score = methode.prosser_unParUn_jusquaFini_time_based_model()#STATIC_TIME
            elif indexMethods == 5:
                methode.trianFirst_space_based_model()
                time, score = methode.prosser_unParUn_jusquaFini_space_based_model()#STatiC_SPACE
            elif indexMethods == 6:
                methode.trainFirst_moyenne_model()
                time, score = methode.prosser_moyenne_model()#Moyenne
            elif indexMethods == 7:
                time, score = methode.prosser_PrivousValue_model()#PrivousValue
            elif indexMethods == 8:
                methode.trainFirst_ISTM()
                time, score = methode.prosser_unParUn_jusquaFini_ISTM_OPM_fraiche(0.9999)#ISTM_OPM_fraiche
                
            nom_file_E_repare_et_E_original =  "resultat/"+ str(nameMethods[indexMethods])+"_E_repare_et_E_original_all"+"_BackTime"+str(backTime)+"_modelTest"+str(test_or_non)+str(trou)
            np.save(nom_file_E_repare_et_E_original, methode.get_E_resultat_E_original_E_a_reparer_all())
                
            print(score**0.5,time)
            
